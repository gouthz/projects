from django.contrib import admin
from django.conf.urls.static import static
from django.urls import include, path
from django.conf import settings
from posts import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('posts/', include('posts.urls')),
    path('login/', views.LoginViewSet.as_view())

    #path('upload/', views.ImageViewSet.as_view(), name='upload'),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)