from rest_framework import generics
from django.http import HttpResponse
from .serializers import PostSerializer,LoginSerializer
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import status, permissions
from .models import Post

class PostList(generics.ListAPIView):
    #queryset = Post.objects.all()
    print("hello")
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    #print(queryset)



class PostDetail(generics.RetrieveAPIView):
    #id1=Post.objects.get(pk=self.kwargs.get('pk'))

    #queryset = Post.objects.filter(id=)
    #print(queryset)
    serializer_class = PostSerializer

    # def get_queryset(self):
    #     """
    #     Get the list of items for this view.
    #     This must be an iterable, and may be a queryset.
    #     Defaults to using `self.queryset`.
    #
    #     This method should always be used rather than accessing `self.queryset`
    #     directly, as `self.queryset` gets evaluated only once, and those results
    #     are cached for all subsequent requests.
    #
    #     You may want to override this if you need to provide different
    #     querysets depending on the incoming request.
    #
    #     (Eg. return a list of items that is specific to the user)
    #     """
    #     print("llllllllllllll",self.kwargs.get('pk'))
    #     print(Post.objects.get(pk=self.kwargs.get('pk')))
    #     return Post.objects.filter(pk=self.kwargs.get('pk'))


    def get_object(self):
        return Post.objects.get(pk=self.kwargs.get('pk'))



class ImageViewSet(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def post(self, request, *args, **kwargs):
        file = request.data['file']
        image = Post.objects.create(image=file)
        return HttpResponse(json.dumps({'message': "Uploaded"}), status=200)

class LoginViewSet(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = LoginSerializer