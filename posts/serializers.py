from rest_framework import serializers
from . import models


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'title', 'content', 'created_at', 'updated_at','name','image',)
        model = models.Post


#class ImageSerializer(serializers.ModelSerializer):
 #   class Meta:
  #      model = models.UploadImageTest
   #     fields = ('name', 'image')

class LoginSerializer(serializers.Serializer):
    """
    Login input check.
    """
    email = serializers.EmailField()
    password = serializers.CharField(style={'input_type': 'password'})