from django.db import models
from rest_framework.authtoken.models import Token

def nameFile(instance, filename):
    return '/'.join(['images', str(instance.name), filename])

#class UploadImageTest(models.Model):
 #   name = models.CharField(max_length=100)
  #  image = models.ImageField(upload_to=nameFile, max_length=254, blank=True, null=True)




class Post(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
   # file = models.FileField(blank=False, null=False)
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=nameFile, max_length=254, blank=True, null=True)
    email=models.CharField(max_length=50)
    password=models.CharField(max_length=50)
    def __str__(self):
        return self.title

